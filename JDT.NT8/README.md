# JDT.NT8
This project contains indicators and strategies which I use to build trading algorithms as well as assist in my discretionary trading.

## Getting Started

[![Getting Started](https://img.youtube.com/vi/OR5rZHtvlbQ/0.jpg)](https://www.youtube.com/watch?v=OR5rZHtvlbQ&list=PLE_aQ1yU56wbCwJEr343wbFhK0fi2zhLv)

## NinjaTrade 8 Features
Coming soon!  This will be a playlist of short videos showing how to use different features of NinjaTrader.  As always, code will be pushed here and available free to use.